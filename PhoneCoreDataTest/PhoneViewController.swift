//
//  PhoneViewController.swift
//  PhoneCoreDataTest
//
//  Created by Sengly Sun on 12/13/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import UIKit

class PhoneViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    

    @IBOutlet var brandLabel: UILabel!
    @IBOutlet var modelLabel: UILabel!
    @IBOutlet var phoneTableView: UITableView!
    var index: Int?
    var userService = UserService()
    var users: [User]?
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneTableView.delegate = self
        phoneTableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        userService = UserService()
//        users = userService.fetchUser(completionHandler: users)
        let element = users?[index!].phones?.count
        return element!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        userService = UserService()
//        users = userService.fetchUser(completionHandler: { (users) in
//
//        })
       
        let cell = phoneTableView.dequeueReusableCell(withIdentifier: "UserDetailCell", for: indexPath)
        return cell
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
