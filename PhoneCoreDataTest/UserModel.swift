//
//  UserModel.swift
//  PhoneCoreDataTest
//
//  Created by Sengly Sun on 12/13/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import Foundation

struct UserModel {
    var name: String
    var age: Int16
    var phone: [PhoneModel]
}

struct PhoneModel {
    var model: String
    var brand: String
}
