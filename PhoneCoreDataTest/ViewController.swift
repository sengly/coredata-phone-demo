//
//  ViewController.swift
//  PhoneCoreDataTest
//
//  Created by Sengly Sun on 12/13/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    

    var userService: UserService!
    var users: [User]?
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userService = UserService()
        tableView.delegate = self
        tableView.dataSource = self
        userService.fetchUser{ (users) in
            self.users = users
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }

    @IBAction func addUserButton(_ sender: Any) {
        print("action")
        userService = UserService()
        let phone1 = PhoneModel(model: "IPhone 7+", brand: "Apple")
        let phone2 = PhoneModel(model: "1280s", brand: "Nokia")
        let phone3 = PhoneModel(model: "S10+", brand: "Samsung")
        let user1 = UserModel(name: "User1", age: 20, phone: [phone1,phone2,phone3])
        let user2 = UserModel(name: "User2", age: 23, phone: [phone3,phone1,phone2])
        userService.createUser(userModel: user1)
        userService.createUser(userModel: user2)
        userService.fetchUser{ (users) in
            self.users = users
            DispatchQueue.main.async {
                self.tableView.reloadData()
                
            }
        }
    }
    
    func setUpTapGesture(){
        let oneTap = UITapGestureRecognizer(target: self, action: #selector(tapGesture(_:)))
        oneTap.numberOfTapsRequired = 1
        oneTap.numberOfTouchesRequired = 1
        tableView.addGestureRecognizer(oneTap)
    }
    
    @objc func tapGesture(_ tapGesture: UITapGestureRecognizer){
        let point = tapGesture.location(in: tableView)
        if let indexPath = tableView?.indexPathForRow(at: point){
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let userDetail = storyboard.instantiateViewController(identifier: "PhoneViewController") as! PhoneViewController
            userDetail.index? = indexPath.row
            navigationController?.pushViewController(userDetail, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"cell", for: indexPath)
        cell.textLabel?.text = users![indexPath.row].name
        cell.detailTextLabel?.text = "Age: \(users![indexPath.row].age)"
        return cell
    }

}

