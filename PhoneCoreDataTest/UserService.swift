//
//  UserService.swift
//  PhoneCoreDataTest
//
//  Created by Sengly Sun on 12/13/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import Foundation
import UIKit

class UserService{
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    func fetchUser(completionHandler: @escaping ([User]) -> Void){
        let users:[User] = try! context.fetch(User.fetchRequest())
        completionHandler(users)
    }
    
//    func addUser(userModel: UserModel){
//        let user = User(context: context)
//
//        user.age = userModel.age
//        user.name = userModel.name
//
//        for i in userModel.phone{
//            let phone = Phone(context: context)
//            phone.brand = i.brand
//            phone.model = i.model
//            user.addToPhones(phone)
//        }
//
//        do {
//            try context.save()
//        } catch  {
//            print(error)
//        }
//        print(fetchUser().count)
//    }
    
    func createUser(userModel: UserModel){
        let user = User(context: context)
        user.age = userModel.age
        user.name = userModel.name
        
        for p in userModel.phone{
            let phone = Phone(context: context)
            phone.brand? = p.brand
            phone.model? = p.model
            user.addToPhones(phone)
        }
        
        try? context.save()
    }
}
